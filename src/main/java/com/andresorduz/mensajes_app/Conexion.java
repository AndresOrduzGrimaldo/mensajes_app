/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.andresorduz.mensajes_app;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author andreso
 */
public class Conexion {
    public Connection get_connection(){
        Connection conexion = null;
        try {
            conexion = DriverManager.getConnection("jdbc:mysql://localhost:8889/mensajes_app", "root","");
            if(conexion != null){
                System.out.println("conexion exitosa");
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return conexion;
    }
}
